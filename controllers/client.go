package controllers

import (
	"bitbucket.com/ZackArts/time-checker/models"
	"github.com/jinzhu/gorm"
	"time"
)

type Client struct {
	DB *gorm.DB
	ts models.Timestamp
}

func (c *Client) Initialize(db *gorm.DB) {
	err := c.DB.Debug().AutoMigrate(&models.Timestamp{}).Error
	if err != nil {
		panic(err)
	}
}

func (c *Client) Start() {
	c.ts.StartTime = time.Now()
}

func (c *Client) End(comment, category string) error {
	c.ts.EndTime = time.Now()
	c.ts.Category = category
	c.ts.Comment = comment
	return c.DB.Debug().Create(&c.ts).Error
}
