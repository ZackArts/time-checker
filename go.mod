module bitbucket.com/ZackArts/time-checker

go 1.14

require (
	github.com/gotk3/gotk3 v0.4.0
	github.com/jinzhu/gorm v1.9.14
)
